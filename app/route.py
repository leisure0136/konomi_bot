from django.http import HttpResponse

import sys
from app.models.GenerateText import GenerateText

def markov(request):
     generator = GenerateText(1)
     resultText = generator.generate()
     return HttpResponse(resultText)
