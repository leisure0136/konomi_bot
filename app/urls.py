from django.conf.urls import url
from django.urls import path 
from . import views

urlpatterns = [
    path(r'index/', views.index, name='index'),
    path(r'tweet/', views.tweet, name='tweet'),
    path(r'home/', views.home, name='home'),
]
