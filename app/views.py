from django.http import HttpResponse

import sys
import json

from app.models.GenerateText import GenerateText
from requests_oauthlib import OAuth1Session
from app.password.password import *

def index(request):
 
     versions = sys.version_info 
     output = str(versions).encode('utf-8')
     
     return HttpResponse(output)

     #generator = GenerateText(1)
     #tweet = generator.generate()

     #twitter = OAuth1Session(CONSUMER_KEY,CONSUMER_SECRET,ACCESS_KEY,ACCESS_SECRET)
     #url = 'https://api.twitter.com/1.1/statuses/update.json'
     #params = {"status" : tweet }
     #req = twitter.post(url, params = params)

     #if req.status_code == 200:
     #    return HttpResponse('OK')
     #else:
     #    return HttpResponse('NG')

def tweet(request):
     generator = GenerateText(1)
     tweet = generator.generate()

     twitter = OAuth1Session(CONSUMER_KEY,CONSUMER_SECRET,ACCESS_KEY,ACCESS_SECRET)
     url = 'https://api.twitter.com/1.1/statuses/update.json'
     params = {"status" : tweet }
     req = twitter.post(url, params = params)

     if req.status_code == 200:
         return HttpResponse('OK')
     else:
         return HttpResponse('NG')

    
def home(request): 
     versions = sys.version_info 
     output = str(versions).encode('utf-8')
     
     return HttpResponse(output)
