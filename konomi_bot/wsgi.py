import os, site, sys
 
site.addsitedir("/root/test_django/lib/python3.6/site-packages")
 
 
sys.path.append("/var/www/cgi-bin/konomi_bot")
sys.path.append("/var/www/cgi-bin/konomi_bot/konomi_bot")
 
from django.core.wsgi import get_wsgi_application
 
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "konomi_bot.settings")
 
application = get_wsgi_application()
