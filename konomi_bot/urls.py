from django.conf.urls import url, include
from django.urls import path, include
from django.contrib import admin
from rest_framework import routers
from django.views.generic import TemplateView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

router = routers.DefaultRouter()
 
urlpatterns = [
    url(r'admin/', admin.site.urls),
    url(r'app/', include('app.urls')),
    url(r'api/', include(router.urls)),
    url('home/', TemplateView.as_view(template_name='index.html'), name='Home'),
]

urlpatterns += staticfiles_urlpatterns()
